﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authentication.Cookies;
using Steeltoe.Security.Authentication.CloudFoundry;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Authentication;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json.Linq;

namespace ApiGeeWebApi.Controllers
{
    [Route("api/[controller]")]
    public class ValuesController : Controller
    {
        #region 
        static HttpClient client = null;
        static string apigeeAccessToken = string.Empty;
        static string apigeeJWT = string.Empty;
        #endregion

        #region Apigee constants
        const string APIGEE_URL = "https://rangajlnath1-test.apigee.net/";
        const string APIGEE_CLIENT_ID = "I5U1fn5hygOAG2nIztTgTSjujwZeAjaj";
        const string APIGEE_CLIENT_SECRET = "6c3knGUXbYdaxDXF";
        const string APIGEE_TOKEN_OAUTH = "/oauth/client_credential/accesstoken?grant_type=client_credentials";
        #endregion

        /// <summary>
        /// ApigeeAccessToken to pass the services.
        /// </summary>
        public static string ApigeeAccessToken
        {
            get
            {
                return apigeeAccessToken;
            }

            set
            {
                apigeeAccessToken = value;
            }
        }


        private IHttpContextAccessor HttpContextAccessor { get; set; }

        public static string ApigeeJWT
        {
            get
            {
                return apigeeJWT;
            }

            set
            {
                apigeeJWT = value;
            }
        }

        public ValuesController(IHttpContextAccessor contextAccessor)
        {
            HttpContextAccessor = contextAccessor;
        }

        /// <summary>
        /// To validate the access token and get the value from the endpoint.
        /// </summary>
        public string GetValueFromApigeeAPI()
        {
            string result = string.Empty;
            if (!string.IsNullOrEmpty(apigeeAccessToken))
            {
                client = new HttpClient();
                client.BaseAddress = new Uri(APIGEE_URL);
                client.DefaultRequestHeaders.Add("Authorization", "Bearer " + apigeeAccessToken);
                result = client.GetStringAsync("/v1/testoauth2").Result;
            }

            return result;
        }

        /// <summary>
        /// To get the token from Apigee API 
        /// </summary>
        //[HttpGet]
        public string GetApigeeAccessToken()
        {
            client = new HttpClient();
            client.BaseAddress = new Uri(APIGEE_URL);
            Dictionary<string, string> authArgs = null;
            authArgs = new Dictionary<string, string>
            {
                { "client_id", APIGEE_CLIENT_ID },
                { "client_secret", APIGEE_CLIENT_SECRET}
            };

            HttpContent content = new FormUrlEncodedContent(authArgs);

            HttpResponseMessage response = client.PostAsync(APIGEE_TOKEN_OAUTH, content).Result;

            if (response.IsSuccessStatusCode)
            {
                apigeeJWT = response.Content.ReadAsStringAsync().Result;
                var jsonObj = JObject.Parse(response.Content.ReadAsStringAsync().Result);
                apigeeAccessToken = (string)jsonObj.SelectToken("access_token");
            }

            client = null;

            return apigeeAccessToken;
        }

        // GET api/values
        [HttpGet]
        public string Get()
        {
            //string token = ApieeExtension.UseApiGEEAuthentication("I5U1fn5hygOAG2nIztTgTSjujwZeAjaj", "6c3knGUXbYdaxDXF");
            // GetApigeeAccessToken();
            string result = string.Empty;
            GetApigeeAccessToken();
            result = string.Format(" ----------------- Apigee JWT generated successfully -------------\r\n{0}\r\n--------------------Access token--------------\r\n{1}\r\n", apigeeJWT, apigeeAccessToken);
            return result;
        }

        // GET api/values/5
        [HttpGet("{AccessToken}")]
        public string Get(string AccessToken)
        {
            string result = string.Empty;
            string clientResult = string.Empty;
            if (!string.IsNullOrEmpty(AccessToken))
            {
                client = new HttpClient();
                client.BaseAddress = new Uri(APIGEE_URL);
                client.DefaultRequestHeaders.Add("Authorization", "Bearer " + AccessToken);
                try
                {
                    clientResult = client.GetStringAsync("/v1/testoauth2").Result;
                    result = string.Format("==========  Value from Apigee API Method ===========\r\n{0}", clientResult);
                }
                catch (Exception)
                {
                    result = string.Format("========== Unauthorised access token ===========\r\n{0}", AccessToken);

                }
                
            }

            
            return result;
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody]string value)
        {
            string token = ApieeExtension.UseApiGEEAuthentication("I5U1fn5hygOAG2nIztTgTSjujwZeAjaj", "6c3knGUXbYdaxDXF");
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
