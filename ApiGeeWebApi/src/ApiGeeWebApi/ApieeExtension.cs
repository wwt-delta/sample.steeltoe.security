﻿using Microsoft.AspNetCore.Builder;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace ApiGeeWebApi
{
    public static class ApieeExtension
    {
        //
        // Summary:
        //     Authenticate users using APIGee
        //
        // Parameters:
        //   app:
        //     The Owin.IAppBuilder passed to the configuration method
        //
        //   appId:
        //     The appId assigned by APIGee
        //
        //   appSecret:
        //     The appSecret assigned by APIGee
        //
        // Returns:
        //     The updated Owin.IAppBuilder
        public static string  UseApiGEEAuthentication(string appId, string appSecret)
        {
            HttpClient client = new HttpClient();
            string token = string.Empty;
            client.BaseAddress = new Uri("https://rangajlnath1-test.apigee.net/");
            var keyValues = new List<KeyValuePair<string, string>>();
            keyValues.Add(new KeyValuePair<string, string>("client_id", appId));
            keyValues.Add(new KeyValuePair<string, string>("client_secret", appSecret));
            HttpContent content = new FormUrlEncodedContent(keyValues);

            HttpResponseMessage response = client.PostAsync("/oauth/client_credential/accesstoken?grant_type=client_credentials", content).Result;

            if (response.IsSuccessStatusCode)
            {
                var jsonObj = JObject.Parse(response.Content.ReadAsStringAsync().Result);
                token = (string)jsonObj.SelectToken("access_token");
            }
            return token;
        }

    }
}
