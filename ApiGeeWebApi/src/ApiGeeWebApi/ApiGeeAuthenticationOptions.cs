﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;


namespace ApiGeeWebApi
{
    public class ApiGeeAuthenticationOptions : OAuthOptions
    {
        public ApiGeeAuthenticationOptions()
        {
            AuthenticationScheme = ApiGeeAuthenticationDefaults.AuthenticationScheme;
            DisplayName = ApiGeeAuthenticationDefaults.DisplayName;
            ClaimsIssuer = ApiGeeAuthenticationDefaults.Issuer;

            CallbackPath = new PathString(ApiGeeAuthenticationDefaults.CallbackPath);

            AuthorizationEndpoint = ApiGeeAuthenticationDefaults.AuthorizationEndpoint;
            TokenEndpoint = ApiGeeAuthenticationDefaults.TokenEndpoint;
            UserInformationEndpoint = ApiGeeAuthenticationDefaults.UserInformationEndpoint;
        }

        /// <summary>
        /// Gets or sets the address of the endpoint exposing
        /// the email addresses associated with the logged in user.
        /// </summary>
        public string UserEmailsEndpoint { get; } = ApiGeeAuthenticationDefaults.UserEmailsEndpoint;
    }
}
